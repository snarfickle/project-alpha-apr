from django.urls import path
from accounts.views import login_view, logout_view, login_signup_view

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", login_signup_view, name="signup"),
]
