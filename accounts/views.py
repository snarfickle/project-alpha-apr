from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from accounts.forms import Login_Form, Login_Signup_Form
from django.contrib.auth.models import User


def login_view(request):
    if request.method == "POST":
        form = Login_Form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/projects/")
    else:
        form = Login_Form()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("/accounts/login/")


def login_signup_view(request):
    if request.method == "POST":
        form = Login_Signup_Form(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("the passwords do not match")
    else:
        form = Login_Signup_Form()
        context = {
            "form": form,
        }
    return render(request, "registration/signup.html", context)
