from django.forms import ModelForm
from projects.models import Project


class Create_Project_Form(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
