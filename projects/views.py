from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from projects.forms import Create_Project_Form


@login_required
def project_list_view(request):
    main_list_view = Project.objects.filter(owner=request.user)

    context = {"projects_object": main_list_view}
    return render(request, "projects/list_view.html", context)


@login_required
def project_detail_view(request, id):
    project = get_object_or_404(Project, id=id, owner=request.user)
    task_list = project.tasks.all()
    context = {"tasks_object": task_list, "projects_object": project}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project_View(request):
    if request.method == "POST":
        form = Create_Project_Form(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect("list_projects")
    else:
        form = Create_Project_Form()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
