from django.forms import ModelForm
from tasks.models import Task


class Create_Task_Form(ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]
        form = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
