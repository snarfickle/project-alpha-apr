from django.shortcuts import render, redirect
from tasks.forms import Create_Task_Form
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def create_task_view(request):
    if request.method == "POST":
        form = Create_Task_Form(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect("list_projects")
    else:
        form = Create_Task_Form()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def task_list_view(request):
    tasks_list = Task.objects.filter(assignee=request.user)
    context = {"tasks_object": tasks_list}
    return render(request, "tasks/my_tasks_list.html", context)
